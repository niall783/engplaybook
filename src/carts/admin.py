from django.contrib import admin

# Register your models here.


from .models import Cart


class CartAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)


admin.site.register(Cart, CartAdmin)