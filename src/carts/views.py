from django.shortcuts import render, redirect

# Create your views here.

from billing.models import BillingProfile, Card

from .models import Cart

from orders.models import Order

from product.models import Product





def payment_plans_view(request):

	#if the user has full access then redirect them away for the payment page
	if request.user.is_authenticated() and request.user.full_access:
		return redirect("pages:home")


	cart_obj, cart_created = Cart.objects.new_or_get(request)

	# this is getting the product object and passing it as context to to the view
	product_qs = Product.objects.all()
	product = product_qs.first() #this contains the price of


	context = {
		"cart":cart_obj,
		"product":product
	}

	return render(request,'carts/payment-plans.html', context)




def cart_update(request):
	product_id = request.POST.get('product_id')
	product_obj = Product.objects.get(id=product_id)
	cart_obj, cart_created = Cart.objects.new_or_get(request)

	cart_obj.products.add(product_obj)

	return redirect("billing:billing-payment-method")





def confirm_purchase_view(request):

	#if the user has full access then redirect them away for the payment page
	if request.user.is_authenticated() and request.user.full_access:
		return redirect("pages:home")




	billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)

	cart_obj, cart_created = Cart.objects.new_or_get(request)	

	if billing_profile is not None:	
		order_obj, order_obj_created = Order.objects.new_or_get(billing_profile, cart_obj)
		



	if request.method == "POST":
		print("Post method activated")


		#creating a charge by making an api call to stripe
		did_charge, crg_msg = billing_profile.charge(order_obj)

		if did_charge:
			print(did_charge)
			#checking if the order is done
			is_done = order_obj.check_done()
			if is_done:
				order_obj.mark_paid(paid_using='stripe')
				#request.session['cart_items'] = 0 #not required for this type of website
				request.user.mark_full_access_paid() 
				del request.session['cart_id']
				return redirect("carts:success", paid_using='stripe')





	
	# what if there is multiple cards
	card_qs = 	Card.objects.filter(billing_profile=billing_profile)

	print(card_qs.count())



	#fix this - user can add loads of cards 
	context = {
		"card": card_qs.first(), # there should only be one card but i am jsut using this here at the moment as a stop gap measure 
		"order": order_obj
	}

	return render(request,'carts/confirm-purchase.html',context)







def payment_success_view(request, paid_using, *args,**kwargs):


	


	if paid_using == 'paypal':	

		print(paid_using)


		billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)

		cart_obj, cart_created = Cart.objects.new_or_get(request)


		if billing_profile is not None:	
	 		order_obj, order_obj_created = Order.objects.new_or_get(billing_profile, cart_obj)



		order_obj.mark_paid(paid_using=paid_using) #either stripe or paypal
				
		request.user.mark_full_access_paid()


	context = {

	}

	return render(request,'carts/payment-success.html', context)






def payment_cancel_view(request, paid_using, *args,**kwargs):

	#if paid_using == 'paypal':	

	context = {

	}

	return render(request,'carts/payment-cancel.html', context)







# def sub_category_list_view(request, category , *args, **kwargs):
# 	queryset = Categories.objects.get(slug=category).sub_category.all()

# 	print(queryset)

# 	context = {
# 		"object_list":queryset
# 	}	
	
# 	return render(request,"pages/sub_category_list_view.html",context)






