from django.conf.urls import url

from .views import payment_plans_view, cart_update, confirm_purchase_view, payment_cancel_view, payment_success_view
# url(r'^$',views.IndexView.as_view(), name='index'),


app_name = 'carts'
urlpatterns = [
		url(r'^payment-plans/$', payment_plans_view, name="payment-plan"),
		url(r'^update-cart/$', cart_update, name="update"),
		url(r'^confirm-purchase/$', confirm_purchase_view, name="confirm_purchase_view"),
		url(r'^payment-success/(?P<paid_using>[\w-]+)/cancel/$', payment_cancel_view, name="cancel"),
		url(r'^payment-success/(?P<paid_using>[\w-]+)/$', payment_success_view, name="success"),

		

		#https://www.engineeringstudentplaybook.com/carts/payment-success/paypal

		#http://127.0.0.1:8000/carts/payment-success/paypal/cancel
		#https://www.engineeringstudentplaybook.com/carts/payment-success/paypal/cancel


	
]