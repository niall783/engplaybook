from django.contrib import admin

from .models import Product


class ProductAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)

admin.site.register(Product, ProductAdmin)


