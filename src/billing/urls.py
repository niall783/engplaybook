from django.conf.urls import url

from .views import payment_method_view, payment_method_createview

# url(r'^$',views.IndexView.as_view(), name='index'),


app_name = 'billing'
urlpatterns = [
		url(r'^payment-method/$', payment_method_view, name="billing-payment-method"),
		url(r'^payment-method/create/$', payment_method_createview, name="billing-payment-method-endpoint"),
		
]