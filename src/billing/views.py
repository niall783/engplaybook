from django.conf import settings
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.utils.http import is_safe_url



# Create your views here.

import stripe

STRIPE_SECRET_KEY = getattr(settings, "STRIPE_SECRET_KEY", "sk_test_nIVx9In87TrlsSDleetPEla2")
STRIPE_PUB_KEY = getattr(settings, "STRIPE_PUB_KEY", 'pk_test_32uQUBQ3iaHdu2cBYSwKZV3Z')
stripe.api_key = STRIPE_SECRET_KEY

from .models import BillingProfile, Card

from carts.models import Cart

from assets.models import Asset


def payment_method_view(request):
	# if request.user.is_authenticated():
	# 	billing_profile = request.user.billingprofile
	# 	my_customer_id = billing_profile.customer_id


	#if the user has full access then redirect them away for the payment page
	if request.user.is_authenticated() and request.user.full_access:
		return redirect("pages:home")


	stripe_brand_assets = Asset.objects.get(title='Stripe') 


	


	billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)

	if not billing_profile:
		return redirect("/")
	next_url = '/carts/confirm-purchase'
	next_ = request.GET.get('next')
	if is_safe_url(next_, request.get_host()):
		next_url = next_


	context = {
		"publish_key": STRIPE_PUB_KEY,
		"next_url": next_url,
		"objects": stripe_brand_assets
	}

	# return render(request, 'billing/payment-method.html', {"publish_key": STRIPE_PUB_KEY, "next_url": next_url})
	return render(request, 'billing/payment-method.html', context)








# def payment_method_createview(request):
# 	if request.method == "POST" and request.is_ajax():
# 		billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
# 		if not billing_profile:
# 			return HttpResponse({"message":"Cannot find this user"}, status_code=401)


# 		token = request.POST.get("token")
# 		if token is not None:
# 			customer = stripe.Customer.retrieve(billing_profile.customer_id)
# 			card_response = customer.sources.create(source=token)
# 			new_card_obj = Card.objects.add_new(billing_profile, card_response)
# 			#print(card_response)
# 			return JsonResponse({"message":"Success! Your card was added."})


# 	return HttpResponse("Error", status_code=401)


def payment_method_createview(request):
	if request.method == "POST" and request.is_ajax():
		billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
		if not billing_profile:
			return HttpResponse({"message": "Cannot find this user"}, status_code=401)
		token = request.POST.get("token")
		if token is not None:

			#this checks to see if the user already has a card saved - if so then delete them before adding another one
			current_cards_qs = Card.objects.filter(billing_profile=billing_profile)
			if current_cards_qs.exists():
				current_cards_qs.delete()

			new_card_obj = Card.objects.add_new(billing_profile, token)
			
		return JsonResponse({"message": "Success! Your card was added."})
	return HttpResponse("error", status_code=401)



















