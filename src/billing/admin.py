from django.contrib import admin

from .models import BillingProfile, Card, Charge


class BillingProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)

admin.site.register(BillingProfile, BillingProfileAdmin)



class CardAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)

admin.site.register(Card, CardAdmin)



class ChargeAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)

admin.site.register(Charge, ChargeAdmin)
