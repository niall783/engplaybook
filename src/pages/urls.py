from django.conf.urls import url

from .views import (home_page, 
					about_page, 
					contact_page,
					pricing_page, 
					connect_page,
					privacy_policy_page,
					sub_category_list_view,
					tutorials_list_view,
					TutorialDetailView,
					TutorialFileDownloadView)



#app_name = 'pages'
urlpatterns = [
	url(r'^$',home_page, name='home'),
	url(r'^about/$',about_page, name='about'),
	url(r'^contact/$',contact_page, name='contact'),
	url(r'^connect/$',connect_page, name='connect'),
	url(r'^pricing/$',pricing_page, name='pricing'),
	url(r'^privacy-policy/$',privacy_policy_page, name='privacy_policy'),
	url(r'^category/(?P<category>[\w-]+)/$',sub_category_list_view, name='sub_category_list'),
	url(r'^category/sub-category/(?P<sub_category>[\w-]+)/$',tutorials_list_view, name='tutorials_list'),
	url(r'^category/sub-category/tutorial/(?P<slug>[\w-]+)/$',TutorialDetailView.as_view(), name='tutorial'),
	url(r'^category/sub-category/tutorial/file/(?P<slug>[\w-]+)/(?P<pk>\d+)/$',TutorialFileDownloadView.as_view(), name='download'),
	#url(r'^tutorial/$',tutorial_view, name='tutorial_view'),
]

#/category/cat/excel-tut-1