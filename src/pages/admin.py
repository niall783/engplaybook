from django.contrib import admin

# Register your models here.
from .models import Tutorial, TutorialContent, TutorialFile, SubCategory, Categories




class TutorialFileInline(admin.TabularInline):
	model 		= TutorialFile
	extra = 1

class TutorialContentInline(admin.TabularInline):
	model 		= TutorialContent
	extra = 1


class TutorialAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)
    inlines = [TutorialContentInline, TutorialFileInline]

    search_fields = ('title',)

    list_display = ('title', 'order')

    #list_filter = ('order',)

admin.site.register(Tutorial, TutorialAdmin)	











class SubCategoryAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)

admin.site.register(SubCategory, SubCategoryAdmin)


class CategoriesAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)

admin.site.register(Categories, CategoriesAdmin)




