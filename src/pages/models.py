from django.db import models

# Create your models here.
import random
import os
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save
from django.urls import reverse

from engplaybook.utils import unique_slug_generator, get_filename 

from engplaybook.aws.download.utils import AWSDownload
from engplaybook.aws.utils import ProtectedS3Storage



#this function returns the filename and the extension
def get_filename_ext(filepath):
	base_name = os.path.basename(filepath)
	name, ext = os.path.splitext(base_name)
	return name, ext


def upload_image_path(intance, filename):
	#print(intance)
	#print(filename)
	new_filename = random.randint(1,3910209312)
	name, ext = get_filename_ext(filename)
	final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
	return "image/{new_filename}/{final_filename}".format(new_filename=new_filename, final_filename=final_filename)













class SubCategory(models.Model):
	#sub_category 		= models.CharField(blank=True, null=True, max_length=120)
	title				= models.CharField(blank=True, null=True, max_length=120)
	slug				= models.SlugField(blank=True, unique=True)
	image 				= models.ImageField(upload_to=upload_image_path, null = True, blank=True)
	short_description 	= models.CharField(blank=True, null=True, max_length=120)
	is_sub_category		= models.BooleanField(default=True)
	updated     		= models.DateTimeField(auto_now=True)
	timestamp   		= models.DateTimeField(auto_now_add=True)



	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse("pages:tutorials_list", kwargs={"sub_category": self.slug})
		

def sub_category_pre_save_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = unique_slug_generator(instance)

pre_save.connect(sub_category_pre_save_receiver, sender=SubCategory)








class Categories(models.Model):
	title 				= models.CharField(blank=True, null=True, max_length=120)
	slug				= models.SlugField(blank=True, unique=True)
	sub_category		= models.ManyToManyField(SubCategory, blank=True)
	is_category 		= models.BooleanField(default=True)
	updated     		= models.DateTimeField(auto_now=True)
	timestamp   		= models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse("pages:sub_category_list", kwargs={"category": self.slug})
		

def categories_pre_save_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = unique_slug_generator(instance)

pre_save.connect(categories_pre_save_receiver, sender=Categories)











class TutorialQuerySet(models.query.QuerySet):
	def search(self,query):
		lookups = (Q(title__icontains=query) |
				  Q(tag__title__icontains=query))
		return self.filter(lookups).distinct()

	def featured(self):
		return self.filter(featured=True)



		




#custom model manager
class TutorialManager(models.Manager):

	def get_queryset(self):
		return TutorialQuerySet(self.model, using=self._db)

	def search(self,query):
		return self.get_queryset().search(query)

	def featured(self): #Tutorial.object.featured
		return self.get_queryset().featured()








class Tutorial(models.Model):
	title 				= models.CharField(blank=True, null=True, max_length=120)
	slug 				= models.SlugField(blank=True, unique=True)
	order 				= models.IntegerField(blank=True, null=True)
	image 				= models.ImageField(upload_to=upload_image_path, null = True, blank=True)
	short_description	= models.TextField(blank=True, null=True)
	featured			= models.BooleanField(default=False)
	is_free				= models.BooleanField(default=False)
	category 			= models.ForeignKey(Categories, blank=True, null=True)
	sub_category 		= models.ForeignKey(SubCategory, blank=True, null=True)
	updated 			= models.DateTimeField(auto_now=True, auto_now_add=False)
	timestamp			= models.DateTimeField(auto_now=False, auto_now_add=True)

	

	objects = TutorialManager()

	def get_absolute_url(self):
		#return "/products/{slug}/".format(slug=self.slug)
		return reverse("pages:tutorial", kwargs={"slug": self.slug})


	def __str__(self):
		return self.title


	@property
	def name(self):
		return self.title

	def get_downloads(self):
		qs = self.tutorialfile_set.all()
		return qs

	def get_content(self):
		qs = self.tutorialcontent_set.all()
		#the qs is reversed so that the objects are ordered in the order that they appear in the admin
		return reversed(qs)




def tutorial_category_pre_save_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = unique_slug_generator(instance)

	#this updates the order field so that that the new turotial appears last
	if instance.order is None and instance.sub_category is not None:
		sub_category = instance.sub_category
		qs = Tutorial.objects.filter(sub_category=instance.sub_category).order_by('order')
		# the the qs does not exist then this means that it is the first tutorial in a particular sub categoy.
		#Therefore, if the the qs doesn't exist then set the order equal to one.
		if qs.exists():
			last_order_number = qs.last().order
			instance.order = last_order_number + 1
		else:
			instance.order = 1 


pre_save.connect(tutorial_category_pre_save_receiver, sender=Tutorial)






# inlines for tutorial content
class TutorialContent(models.Model):
	tutorial 				= models.ForeignKey(Tutorial)
	video_link				= models.TextField(blank=True, null=True)
	video_link_col_params   = models.CharField(max_length=120, default="col-12 col-md-10 mx-auto")
	image 					= models.ImageField(upload_to=upload_image_path, null = True, blank=True)
	image_col_params		= models.CharField(max_length=120, default="col-12 col-md-5 mx-auto")
	text					= models.TextField(blank=True, null=True)	
	text_col_params			= models.CharField(max_length=120, default="col-12 mx-auto")
	html_text				= models.TextField(blank=True, null=True)	
	html_text_col_params	= models.CharField(max_length=120, default="col-12 mx-auto")

	def __str__(self):
		return self.tutorial.title













#inlines for downloadable content
#this is in relation to uploading downloadable resources associated with the tutorial
def upload_tutorial_file_location(instance, filename):
	slug = instance.tutorial.slug
	# this try block stuff is to ensure that the file upload name in unique
	id_ = 0

	id_ = instance.id

	if id_ is None:
		Klass = instance.__class__
		qs = Klass.objects.all().order_by('-pk')
		if qs.exists():
			id_ = qs.first().id + 1
			
		else:
			id_ = 0

	if not slug:
		slug = unique_slug_generator(instance.tutorial)
	location = "tutorial/{slug}/{id}/".format(slug=slug, id=id_)
	return location + filename





#this is in relation to uploading downloadable resources associated with the tutorial
class TutorialFile(models.Model):
	tutorial 				= models.ForeignKey(Tutorial)
	name 					= models.CharField(max_length=120, null=True, blank=True)
	thumbnail				= models.ImageField(upload_to=upload_image_path, null = True, blank=True)		
	file 					= models.FileField(
								upload_to=upload_tutorial_file_location,
	 							storage=ProtectedS3Storage(), #FileSystemStorage(location=settings.PROTECTED_ROOT))
								)
	is_free 				= models.BooleanField(default=False)
	


	def __str__(self):
		return str(self.file.name)


	@property
	def display_name(self):
		og_name = get_filename(self.file.name) #original name
		if self.name:
			return self.name
		return og_name
		
	



	def get_default_url(self):
		return self.tutorial.get_absolute_url()


	def generate_download_url(self):
		bucket = getattr(settings, 'AWS_STORAGE_BUCKET_NAME')
		region = getattr(settings, 'S3DIRECT_REGION')
		access_key = getattr(settings, 'AWS_ACCESS_KEY_ID')
		secret_key = getattr(settings, 'AWS_SECRET_ACCESS_KEY')
		if not secret_key or not access_key or not bucket or not region:
			return "/not-found"
		PROTECTED_DIR_NAME = getattr(settings, 'PROTECTED_DIR_NAME', 'protected')
		path = "{base}/{file_path}".format(base=PROTECTED_DIR_NAME, file_path=str(self.file))

		aws_dl_object =  AWSDownload(access_key, secret_key, bucket, region)
		file_url = aws_dl_object.generate_url(path, new_filename=self.display_name)

		
		return file_url


	def get_download_url(self):
		return reverse("pages:download", kwargs={"slug": self.tutorial.slug, "pk": self.pk})

	
