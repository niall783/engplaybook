# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-11-21 19:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0005_subcategory_is_sub_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tutorial',
            name='slug',
            field=models.SlugField(blank=True, unique=True),
        ),
    ]
