import os
from django.views.generic import ListView, DetailView, View
from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404, HttpResponse, HttpResponseRedirect

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from django.contrib import messages

from wsgiref.util import FileWrapper
from django.conf import settings

from mimetypes import guess_type

from assets.models import Asset, Quote

from product.models import Product


from .models import Tutorial, Categories, SubCategory, TutorialFile







def home_page(request):

	user = request.user

	featured_tutorials_qs = Tutorial.objects.featured()

	home_page_assets = Asset.objects.get(title='HomePage')

	quote = Quote.objects.get(quote_type='home_page_quote')

	# this is getting the product object and passing it as context to to the view
	product_qs = Product.objects.all()
	product = product_qs.first()

	context = {
		"featured_tutorials_qs":featured_tutorials_qs,
		"home_page_assets":home_page_assets,
		"quote":quote,
		"product":product,
	}

	# if request.user.is_authenticated():
	# 	context["premium_content"] = "Oh yeah premium content"

	return render(request,"pages/home_page.html",context)



def about_page(request):


	quote = Quote.objects.get(quote_type='home_page_quote')

	about_page_assets = Asset.objects.get(title='AboutPage')

	

	context = {
		"quote":quote,
		"about_page_assets":about_page_assets,
	}

	return render(request,"pages/about_page.html",context)




def privacy_policy_page(request):

	privacy_policy_page_assets = Asset.objects.get(title='PrivacyPolicy')

	context = {
		"asset":privacy_policy_page_assets,
		
	}

	return render(request,"pages/privacy_policy_page.html",context)





def pricing_page(request):

	user = request.user

	home_page_assets = Asset.objects.get(title='HomePage')

	quote = Quote.objects.get(quote_type='home_page_quote')

	# this is getting the product object and passing it as context to to the view
	product_qs = Product.objects.all()
	product = product_qs.first()

	context = {
		"home_page_assets":home_page_assets,
		"quote":quote,
		"product":product,
	}

	return render(request,"pages/pricing_page.html",context)






def connect_page(request):

	quote = Quote.objects.get(quote_type='home_page_quote')

	context = {
		"quote":quote,
	}

	return render(request,"pages/connect_page.html",context)








def contact_page(request):



	context = {
		"title":"Contact Page",
		"content": "Enter Contact us Related Content"
	}

	return render(request,"pages/contact_page.html",context)





#class SubCategoryListView(ListView):
	#queryset = 



# class SubCategoryListView(ListView):
# 	queryset = Tutorial.objects.all()
# 	template_name = "pages/sub_category_list_view.html"

	#object_list is the default content object

def sub_category_list_view(request, category , *args, **kwargs):
	queryset = Categories.objects.get(slug=category).sub_category.all()

	# print(queryset)

	context = {
		"object_list":queryset
	}	
	
	return render(request,"pages/sub_category_list_view.html",context)



def tutorials_list_view(request, sub_category, *args, **kwargs):
	#getting the sub category object to put in to the filter paramaeer below
	sub_category_obj = SubCategory.objects.get(slug=sub_category)


	queryset = Tutorial.objects.filter(sub_category=sub_category_obj).order_by('order')

	order_number = 1
	for obj in queryset:
		obj.order = order_number
		obj.save()
		order_number = order_number + 1




	queryset = Tutorial.objects.filter(sub_category=sub_category_obj).order_by('order')

	

	#this section is all about changing the order that the tutorial objects are displayed.
	if request.method == "POST":

		slug = request.POST.get('tutorial_slug')

		obj1 = Tutorial.objects.get(slug=slug)

		if obj1.order != 1:
			#getting the current obj order for the obj that is to be moved up
			obj1_current_order = obj1.order
			obj1_new_order = obj1_current_order - 1
			obj1.order = obj1_new_order

			
			#getting the obj that is to be moved down into the original position of obj1
			obj2 = Tutorial.objects.get(sub_category=obj1.sub_category, order=obj1_new_order)
			obj2.order = obj1_current_order

			#saving the objects with their new orders
			obj1.save()
			obj2.save()



	
	
	context = {
		"object_list":queryset,
	}	
	return render(request,"pages/tutorials_list_view.html",context)







class TutorialFileDownloadView(View):
	def get(self,request, *args, **kwargs):
		slug = kwargs.get('slug')
		pk = kwargs.get('pk')



		downloads_qs = TutorialFile.objects.filter(pk=pk, tutorial__slug=slug)



		if downloads_qs.count() != 1:
			raise Http404("Download not found")

		download_obj = downloads_qs.first()



		#if the downloadable resource is not free and the user is not logged in then redirect them to the login page
		if not download_obj.is_free and not request.user.is_authenticated():
			print("User is not logged in")
			if request.user.is_authenticated() and not request.user.full_access:
				print("The user is not logged in and they do not have full access")
				return redirect("account:login")
			return redirect("account:login")	


		aws_filepath = download_obj.generate_download_url()

		return HttpResponseRedirect(aws_filepath)
		# file_root = settings.PROTECTED_ROOT
		# filepath = download_obj.file.path # .url /media/
		# final_filepath = os.path.join(file_root, filepath) # where the file is stored
		# with open(final_filepath, 'rb') as f:
		# 	wrapper = FileWrapper(f)
		# 	mimetype = 'application/force-download'
		# 	guessed_mimetype = guess_type(filepath)[0] # filename.mp4
		# 	if guessed_mimetype:
		# 		mimetype = guessed_mimetype
		# 	response = HttpResponse(wrapper, content_type=mimetype)
		# 	response['Content-Disposition'] = "attachment;filename=%s" %(download_obj.name)
		# 	response["X-SendFile"] = str(download_obj.name)
		# 	return response
		#return redirect(download_obj.get_default_url())
 










class TutorialDetailView(DetailView):
	queryset = Tutorial.objects.all()
	template_name = "pages/tutorial_view.html"

	

	def get_context_data(self, *args, **kwargs):
		context = super(TutorialDetailView,self).get_context_data(*args, **kwargs)
		instance = context['object'] #getting the tutorial object i.e. the instance of the tutorial
		context['sidebar_qs'] = Tutorial.objects.filter(sub_category=instance.sub_category).order_by('order') # getting a queryset of all the other tutorials with the same subcategory as the instance
		context['sub_category'] = instance.sub_category
		return context


	def get_object(self, *args, **kwargs):
		request = self.request
		user = request.user
		slug = self.kwargs.get('slug')

		#instance = get_object_or_404(Tutorial, slug=slug)
		#return instance

	

		try:
			instance = Tutorial.objects.get(slug=slug)
		except Tutorial.DoesNotExist:
			raise Http404("Not found..")
		except Tutorial.MultipleObjectsReturned:
			qs = Tutorial.objects.filter(slug=slug)
			instance = qs.first()
		except:
			raise Http404("Uhhmmm ")
		return instance


		








# def tutorial_view(request):
# 	topics_qs = Tutorial.objects.all()
# 	content = {
# 		"object_list": topics_qs
# 	}
# 	return render(request, "pages/tutorial_view.html", content)










#old function based view for the topics list pae
# def topics_list_page(request):
# 	topics_qs = Tutorial.objects.all()
# 	context = {
# 		"object_list":topics_qs
# 	}
# 	return render(request,"pages/topics_list_page.html",context)

