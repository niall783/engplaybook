"""engplaybook URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static

from django.contrib.auth.views import LogoutView



urlpatterns = [
    url(r'^search/', include('search.urls', namespace='search')),
	url(r'^', include('pages.urls', namespace='pages')),
	url(r'^account/', include('account.urls', namespace='account')),
    url(r'^account/config/', include('account.passwords.urls')),
    url(r'^billing/', include('billing.urls', namespace='billing')),
    url(r'^carts/', include('carts.urls', namespace='carts')),
    url(r'^admin/', admin.site.urls),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
]


if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
