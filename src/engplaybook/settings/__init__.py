
from .base import *

from .production  import *


# the local file is ignored when I push the updates to heroku becuase of the git ignore file - thats why his works
try:
	from .local import *
except:
	pass
