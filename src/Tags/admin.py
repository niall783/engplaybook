from django.contrib import admin

from .models import Tag



class TagAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)

admin.site.register(Tag, TagAdmin)
