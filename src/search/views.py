from django.shortcuts import render

from django.shortcuts import render
from django.views.generic import ListView
from pages.models import Tutorial


class SearchTutorialView(ListView):
	queryset = Tutorial.objects.all()
	template_name = "search/view.html"

	def get_context_data(self, *args, **kwargs):
		context = super(SearchTutorialView, self).get_context_data(*args, **kwargs)
		query = self.request.GET.get('q')
		context['query'] = query
		return context

	def get_queryset(self, *args, **kwargs):
		request = self.request
		method_dict = request.GET
		query = method_dict.get('q', None) # method_dict['q']  # default is none but could be 'featured' tutorials # this allows you to search using '?q=' in the url bar
		if  query is not "":
			print("query is not none")
			print(query)
			return Tutorial.objects.search(query) # search is a custom function
		print("query is not none")
		print(query)
		return Tutorial.objects.featured()



# class SearchProductView(ListView):
# 	queryset = Product.objects.all()
# 	template_name = "search/view.html"

# 	def get_context_data(self,*args, **kwargs):
# 		context = super(SearchProductView,self).get_context_data(*args,**kwargs)
# 		query = self.request.GET.get('q')
# 		context['query'] = query
# 		#SearchQuery.objects.create(query=query)
# 		return context

# 	def get_queryset(self, *args, **kwargs):
# 		request = self.request
# 		print(request.GET)
# 		query = request.GET.get('q', None) #default is None but could be 'featured' products # this allows you to search using '?q=' in the url bar
# 		if query is not None:
# 			return	Product.objects.search(query) #search is a custom function
# 		return Product.objects.featured()

# 		'''
# 		__icontains = field contains this
# 		__iexact = fields is exactly this

# 		'''