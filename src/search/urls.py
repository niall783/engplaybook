from django.conf.urls import url

from .views import (
		SearchTutorialView
		)



urlpatterns = [
    url(r'^$', SearchTutorialView.as_view(), name='query')
]
