from django.contrib import admin

# Register your models here.


from .models import Order

class OrderAdmin(admin.ModelAdmin):
    readonly_fields = ('paid_at','timestamp','updated',)

admin.site.register(Order, OrderAdmin)



