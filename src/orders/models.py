from decimal import Decimal

from django.db import models

# Create your models here.
from django.utils import timezone

from django.db.models.signals import pre_save, post_save
from engplaybook.utils import unique_order_id_generator

from billing.models import BillingProfile

from carts.models import Cart

ORDER_STATUS_CHOICES = (
		('created','Created'),
		('paid','Paid'),
		('shipped','Shipped'),
		('refunded','Refunded')
	)

PAID_USING_CHOICES = (
		('stripe','Stripe'),
		('paypal','PayPal'),
	)




# to create an order type: order_obj, order_obj_created = Order.objects.new_or_get(billing_profile=billing_profile) - mb


class OrderManagerQuerySet(models.query.QuerySet):
	def by_billing_profile(self, request):
		billing_profile = BillingProfile.objects.new_or_get(self.request)
		return self.get_queryset().filter(billing_profile=billing_profile)


class OrderManager(models.Manager):
	def get_queryset(self):
		return OrderManagerQuerySet(self.model, using=self._db)

	def by_request(self, request):
		return self.get_queryset().by_request(request)

	def new_or_get(self, billing_profile, cart_obj):
		created = False
		qs = self.get_queryset().filter(billing_profile=billing_profile, cart=cart_obj, active=True,status='created')
		if qs.count() == 1:
			obj = qs.first()
		else:
			obj = self.model.objects.create(billing_profile=billing_profile, cart=cart_obj)
			created = True
		return obj, created








class Order(models.Model):
	billing_profile 	= models.ForeignKey(BillingProfile, null=True, blank=True) #null=True, blank=True - Only for now to make migrations
	order_id			= models.CharField(max_length=120, blank=True)
	cart 				= models.ForeignKey(Cart, null=True, blank=True)
	#billing_profile 	=
	#shipping_address	=
	#billing_address	=
	status				= models.CharField(max_length=120, default='created', choices=ORDER_STATUS_CHOICES)
	paid_at				= models.DateTimeField(blank=True, null=True)
	paid_using			= models.CharField(max_length=120, choices=PAID_USING_CHOICES, blank=True, null=True)
	#shipping_total 		= models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
	total 				= models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
	active				= models.BooleanField(default=True)
	updated     		= models.DateTimeField(auto_now=True)
	timestamp   		= models.DateTimeField(auto_now_add=True)
    


	def __str__(self):
		return self.order_id

	objects = OrderManager()



	def update_total(self):
		cart_total = self.cart.total
		#shipping_total = self.shipping_total
		#new_total = math.fsum([cart_total,shipping_total])
		new_total = cart_total
		formatted_total = format(new_total,'.2f')
		self.total = formatted_total
		self.save()
		return new_total

	def check_done(self):
		billing_profile = self.billing_profile
		#shipping_address = self.shipping_address
		#billing_address = self.billing_address
		total = Decimal(self.total) 
		print('Total')
		print(total)
		print(type(total))
		#if billing_profile and shipping_address and billing_address and total > 0:
		if billing_profile and total > 0:
			return True
		return False

	def mark_paid(self, paid_using):
		if self.check_done():
			self.status = "paid"
			self.paid_at = timezone.now()
			self.paid_using=paid_using #either stripe or paypal
			self.save()
		return self.status


def pre_save_create_order_id(sender, instance, *args, **kwargs):
	if not instance.order_id:
		instance.order_id = unique_order_id_generator(instance)

	# what does this do?
	qs = Order.objects.filter(cart=instance.cart).exclude(billing_profile=instance.billing_profile)
	if qs.exists():
		qs.update(active=False)

pre_save.connect(pre_save_create_order_id, sender=Order)



def post_save_cart_total(sender,instance,created, *args, **kwargs):
	if not created:
		cart_obj = instance
		cart_total = cart_obj.total
		cart_id = cart_obj.id
		qs = Order.objects.filter(cart__id=cart_id)
		if qs.count() == 1:
			order_obj = qs.first()
			order_obj.update_total()

post_save.connect(post_save_cart_total, sender=Cart)



def post_save_order(sender, instance, created, *args, **kwargs):
	print("running")
	if created:
		print("Updating... first")
		instance.update_total()

post_save.connect(post_save_order, sender=Order)
