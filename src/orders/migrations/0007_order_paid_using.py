# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-12-29 15:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0006_order_paid_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='paid_using',
            field=models.CharField(blank=True, choices=[('stripe', 'Stripe'), ('paypal', 'PayPal')], max_length=120, null=True),
        ),
    ]
