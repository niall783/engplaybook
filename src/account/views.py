from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.views.generic import CreateView, FormView, DetailView, View
from django.views.generic.edit import FormMixin
from django.utils.decorators import method_decorator
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.http import is_safe_url
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

from engplaybook.mixins import NextUrlMixin, RequestFormAttachMixin

from .forms import LoginForm, RegisterForm, ReactivateEmailForm

from .models import EmailActivation


# jsut to show how to do a login_required decorator on a function based view
# @login_required
# def account_home_view(request):
# 	return render(request, "account/home.html")





class AccountHomeView(LoginRequiredMixin, DetailView):
	template_name = 'account/home.html'
	def get_object(self):
		return self.request.user




class AccountEmailActivateView(FormMixin, View):
	success_url = "/account/login/" # not sure what this does
	form_class = ReactivateEmailForm

	key = None

	def get(self, request,key=None, *args, **kwargs):

		if key is not None:
			self.key = key
			#account has not been activated
			qs = EmailActivation.objects.filter(key__iexact=key)
			confirm_qs = qs.confirmable()
			if confirm_qs.count() == 1:
				obj = confirm_qs.first()
				obj.activate()
				messages.success(request, "Your email has been confirmed. Please login to complete payment.")
				return redirect("/account/login/?next=/carts/payment-plans") #redirect("account:login")
			else:
				#account has been activated
				activated_qs = qs.filter(activated=True)
				if activated_qs.exists():
					reset_link = reverse("password_reset")

					msg = """Your email has already been confirmed, do you need
					to <a href="{link}"> reset your password</a>?
					""".format(link=reset_link)

					messages.success(request, mark_safe(msg))
					return redirect("/account/login/?next=/carts/payment-plans") #redirect("account:login")


		context = {'form': self.get_form(), 'key': key}
		return render(request, 'registration/activation-error.html', context)


	def post(self, request, *args, **kwargs):
		#create form to receive an email
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)


	def form_valid(self, form):
		msg = """Activation link sent, please check your email"""
		request = self.request
		messages.success(request, msg)
		email = form.cleaned_data.get("email")
		obj = EmailActivation.objects.email_exists(email).first()
		user = obj.user

		#checking if there are any email activations associated with this email
		qs = EmailActivation.objects.filter(email=email)
		if qs.exists:
			#if the the qs exists then delete the records.
			qs.delete()

		new_activation = EmailActivation.objects.create(user=user, email=email)
		new_activation.send_activation()
		return super(AccountEmailActivateView, self).form_valid(form)


	def form_invalid(self, form):
		context = {'form': form, "key": self.key }
		return render(self.request, 'registration/activation-error.html', context)





class LoginView(NextUrlMixin, RequestFormAttachMixin, FormView):
	form_class = LoginForm
	success_url = '/'
	template_name = "account/login.html"
	default_next ='/'

	def form_valid(self, form):
		next_path = self.get_next_url()
		return	redirect(next_path)
		
		



# # i believe that this is not used and the one above is used instead
# def login_page(request):
# 	form = LoginForm(request.POST or None)
# 	context = {
# 		"form":form
# 	}
# 	if form.is_valid():
# 		username = form.cleaned_data.get("username")
# 		password = form.cleaned_data.get("password")
# 		user = authenticate(request, username=username, password=password)
# 		if user is not None:
# 			login(request, user)
# 			#redirect to success page
# 			#context['form'] = LoginForm()
# 			return	redirect("/")
# 		else:
# 			print("error")
	
# 	return render(request, "account/login.html",context)




class RegisterView(CreateView):
	form_class = RegisterForm
	template_name = 'account/register.html'
	success_url = '/account/check-your-email' #'/billing/payment-method' #this doesn't work - the person seems to need to be logged in first



# User = get_user_model()

# def register_page(request):
# 	form = RegisterForm(request.POST or None)
# 	context = {
# 		"form":form
# 	}
# 	if form.is_valid():
# 		form.save()
# 		# print(form.cleaned_data)
# 		# username = form.cleaned_data.get("username")
# 		# email	 = form.cleaned_data.get("email")
# 		# password = form.cleaned_data.get("password")
		
# 		new_user = User.objects.create_user(username, email, password)
# 	return render(request, "account/register.html",context)




def check_your_email(request):
	
	context = {
		
	}

	# if request.user.is_authenticated():
	# 	context["premium_content"] = "Oh yeah premium content"

	return render(request,"account/check_your_email.html",context)