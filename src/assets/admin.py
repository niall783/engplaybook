from django.contrib import admin

# Register your models here.

from .models import Asset, Quote

class AssetAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)

admin.site.register(Asset, AssetAdmin)



class QuoteAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)

admin.site.register(Quote, QuoteAdmin)