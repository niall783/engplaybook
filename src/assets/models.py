from django.db import models

import random
import os


#this function returns the filename and the extension
def get_filename_ext(filepath):
	base_name = os.path.basename(filepath)
	name, ext = os.path.splitext(base_name)
	return name, ext


def upload_image_path(intance, filename):
	#print(intance)
	#print(filename)
	new_filename = random.randint(1,3910209312)
	name, ext = get_filename_ext(filename)
	final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
	return "image/{new_filename}/{final_filename}".format(new_filename=new_filename, final_filename=final_filename)







class Asset(models.Model):
	title				= models.CharField(blank=True, null=True, max_length=120)
	image1 				= models.ImageField(upload_to=upload_image_path, null = True, blank=True)
	content1 			= models.TextField(blank=True, null=True)
	image2 				= models.ImageField(upload_to=upload_image_path, null = True, blank=True)
	content2 			= models.TextField(blank=True, null=True)
	image3 				= models.ImageField(upload_to=upload_image_path, null = True, blank=True)
	content3 			= models.TextField(blank=True, null=True)
	image4 				= models.ImageField(upload_to=upload_image_path, null = True, blank=True)
	content4 			= models.TextField(blank=True, null=True)
	image5 				= models.ImageField(upload_to=upload_image_path, null = True, blank=True)
	content5 			= models.TextField(blank=True, null=True)
	image6 				= models.ImageField(upload_to=upload_image_path, null = True, blank=True)
	content6 			= models.TextField(blank=True, null=True)

	updated     		= models.DateTimeField(auto_now=True)
	timestamp   		= models.DateTimeField(auto_now_add=True)



	def __str__(self):
		return self.title






class Quote(models.Model):
	quote 			= models.TextField(blank=True, null=True)
	author			= models.CharField(blank=True, null=True, max_length=120)
	source 			= models.CharField(blank=True, null=True, max_length=120)
	quote_type		= models.CharField(blank=True, null=True, max_length=120)
	updated     	= models.DateTimeField(auto_now=True)
	timestamp   	= models.DateTimeField(auto_now_add=True)


	def __str__(self):
		return self.author

